import pygame
import sys
import time
import math

def main():
	pygame.init()
	clock = pygame.time.Clock()
	fps = 60
	size = [900, 500]
	passive = 0
	click_counter = 1
	
	click_upgrade = 10
	click_farm = 100

	click_lvl = 1
	farm_lvl = 0

	click_scale = 1
	farm_scale = 50

	total = 0
	golden_apple = 0.0

	screen = pygame.display.set_mode(size)
	pygame.display.set_caption('Apple Clicker')

	money = '0'
	WHITE = (255, 255, 255)
	BLACK = (0, 0, 0)
	apple = pygame.image.load('nobite.jpg')
	apple = pygame.transform.scale(apple, (200, 200))

	basicFont = pygame.font.SysFont(None, 48)
	upgradesFont = pygame.font.SysFont(None, 24)
	upgrade_click = pygame.Rect(0, 0, 350, 75)
	upgrade_farm = pygame.Rect(0, 90, 350, 75)
	prestige_box = pygame.Rect(0, 180, 350, 75)
	alpha = int(round(time.time() * 1000))
	while True:
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				return False
			if event.type == pygame.MOUSEBUTTONDOWN:
				mouse_pos = event.pos
				if upgrade_click.collidepoint(mouse_pos):
					if(float(money)>=click_upgrade):
						click_counter += click_scale
						money = str(float(money) - click_upgrade)
						click_upgrade = click_upgrade*1.5
						click_scale = click_scale*1.05
						click_lvl += 1
						if(click_lvl%5==0):
							click_counter = click_counter*2
				elif upgrade_farm.collidepoint(mouse_pos):
					if(float(money)>=click_farm):
						passive += farm_scale
						money = str(float(money) - click_farm)
						click_farm = click_farm*1.5
						farm_scale = farm_scale*1.05
						farm_lvl += 1
						if(farm_lvl%5==0):
							passive = passive*2
				elif prestige_box.collidepoint(mouse_pos):
					golden_apple = golden_apple+(math.sqrt(math.sqrt(total)/2) * math.sqrt(0.01))
					passive = 0
					click_counter = 1
	
					click_upgrade = 10
					click_farm = 100

					click_lvl = 1
					farm_lvl = 0

					click_scale = 1
					farm_scale = 50

					total = 0
					money = 0
				else:
					total = (total + click_counter)*(((golden_apple*2)/100)+1)
					money = str((float(money) + click_counter)*(((golden_apple*2)/100)+1))
		if((int(round(time.time() * 1000)))-alpha >= 1000 and passive != 0):
			money = str((float(money) + passive) *(((golden_apple*2)/100)+1))
			total = (total + passive) * (((golden_apple*2)/100)+1)
			alpha = int(round(time.time() * 1000))
		text = basicFont.render("{:.2f}".format(float(money)), True, BLACK)
		screen.fill(WHITE)
		screen.blit(apple, (500, 200))
		screen.blit(text, (600, 100))
		screen.blit(upgradesFont.render("Golden Apples {:.2f}".format(golden_apple), True, BLACK), (600, 1))
		screen.blit(upgradesFont.render("Passive Income: {:.2f}/s".format(passive), True, BLACK), (540, 150))
		screen.blit(upgradesFont.render("Upgrade click for {:.2f} points Level {}".format(click_upgrade, click_lvl), True, BLACK), (20,30))
		screen.blit(upgradesFont.render("Upgrade farm for {:.2f} points Level {}".format(click_farm, farm_lvl), True, BLACK), (20,120))
		screen.blit(upgradesFont.render("Presitge start from the top", True, BLACK), (20,210))
		pygame.draw.rect(screen, [244, 65, 65], upgrade_click, 2)
		pygame.draw.rect(screen, [244, 65, 65], upgrade_farm, 2)
		pygame.draw.rect(screen, [244, 65, 65], prestige_box, 2)
		pygame.display.update()
		clock.tick(fps)

	pygame.quit()
	sys.exit
main()
